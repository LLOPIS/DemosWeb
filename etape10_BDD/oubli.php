<?php session_start();

/* Chaque élément du formulaire est récupéré dans le tableau associatif $_GET */

/* si on a essayé d'accéder au script sans passer par le formulaire, on redirige vers une image appropriée et on arrête l'exécution */
if(!isset($_GET['oublier'])){
    // http://php.net/manual/fr/function.header.php
    header('Location: http://adhesifservice.fr/295-thickbox_default/panneau-danger-acces-interdit-au-personnel-non-autorise.jpg');
    // https://stackoverflow.com/questions/2747791/why-i-have-to-call-exit-after-redirection-through-headerlocation-in-php
    exit();
}

if ($_GET['oublier']=="ok" && isset($_SESSION['dejavu'])){
    unset($_SESSION['dejavu']);
}

/* on renvoie à la page appelante */
header("Location: ".$_SERVER['HTTP_REFERER']);
?>
