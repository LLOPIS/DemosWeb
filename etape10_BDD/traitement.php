<?php session_start();
/* inclusion des fonctions de validation */
require_once("./funcs_validation.inc.php");
    /* inclusion de la fonction connectMySQL() */
require_once("./connectMySql.inc.php");

/* Chaque élément du formulaire est récupéré dans le tableau associatif $_POST */

/* si on a essayé d'accéder au script sans passer par le formulaire, on redirige vers une image appropriée et on arrête l'exécution */
if(!isset($_POST['valid_contact'])){
    // http://php.net/manual/fr/function.header.php
    header('Location: http://adhesifservice.fr/295-thickbox_default/panneau-danger-acces-interdit-au-personnel-non-autorise.jpg');
    // https://stackoverflow.com/questions/2747791/why-i-have-to-call-exit-after-redirection-through-headerlocation-in-php
    exit();
}

/* On instancie une variable de session pour la gestion des erreurs. Au départ le formulaire est considéré comme valide */
$_SESSION['form_erreur']=FALSE;

// récupération du nom depuis le formulaire
$nom_dangereux=$_POST['nom'];

// validation du nom avec la fonction validerNom
$nom_securise=validerNom($nom_dangereux);

// récupération du numéro de téléphone
$tel_dangereux=$_POST['telnum'];

// validation avec validerTel
$tel_securise=validerTel($tel_dangereux);

// récupération de l'email
$courriel_dangereux=$_POST['courriel'];

// validation avec validerTel
$courriel_securise=validerCourriel($courriel_dangereux);

// récupération du message
$message_dangereux=$_POST['message'];
$message_securise=htmlspecialchars($message_dangereux);

if($_SESSION['form_erreur']){
    // s'il y a des erreurs ailleurs dans le formulaire il faut le réafficher, en enlevant les éventuels éléments d'attaque XSS
    $_SESSION['message']= $message_securise;
    // on retourne au formulaire
    header("Location: ".$_SERVER['HTTP_REFERER']."#contactForm");
    exit();
}


// si tout va bien, on élimine les variables de session, car le formulaire a été validé. on ne concerve que le nom qui est affiché dans le message de confirmation.
unset($_SESSION['tel']);
unset($_SESSION['courriel']);
unset($_SESSION['message']);
// on insère dans la base de données
try {
    $conn = connectMySQL();
    $stmt=$conn->prepare ('INSERT INTO `messages` (`msg.nomprenom`, `msg.telephone`, `msg.email`, `msg.texte`) VALUES (:nomprenom, :tel, :mail, :msg);');

    //affectation des valeurs
    $stmt -> bindParam(':nomprenom', $nom_securise);
    $stmt -> bindParam(':tel', $tel_securise);
    $stmt -> bindParam(':mail', $courriel_securise);
    $stmt -> bindParam(':msg', $message_securise);

    // execution de la requête
    $res=$stmt->execute();

    $conn = null;
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    echo $e->getTraceAsString();
    die();
}


/* on renvoie à la page appelante pour indiquer le succès */
   header("Location: ".$_SERVER['HTTP_REFERER']."#contactForm");
?>
